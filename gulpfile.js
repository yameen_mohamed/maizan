var gulp = require('gulp');
var minify =  require('gulp-minify-css');
var concat =  require('gulp-concat');
var gap = require('gulp-append-prepend');


gulp.task('default', function(){
	gulp.src([
		'wp-content/themes/organic-purpose/src/css/style.css',
		'wp-content/themes/organic-purpose/src/css/style-mobile.css'
		])
		.pipe(minify())
		.pipe(concat('style.css'))
		.pipe(gap.prependFile('wp-content/themes/organic-purpose/src/theme.css'))
		.pipe(gulp.dest('wp-content/themes/organic-purpose'))
});