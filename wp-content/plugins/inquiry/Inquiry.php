<?php
/*
Plugin Name: General Inquiry Form
Plugin URI: N/A
Description: A simple inquiry form
Author: Yameen Mohamed
Version: 10.0
Author URI: mailto:yaamynu@gmail.com
*/


function subscribe_link_shortcode() {
	if(isset($_POST['inquiry']))
	{
		require 'class.smtp.php';
		require 'class.phpmailer.php';

		$mail = new PHPMailer;

		//$mail->SMTPDebug = 3;                               // Enable verbose debug output

		$mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'mail.ics.com.mv';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true; 
		$mail->SMTPAutoTLS = false; 
		$mail->SMTPSecure = false;                              // Enable SMTP authentication
		$mail->Username = 'support@ics.com.mv';                 // SMTP username
		$mail->Password = 'Sup@123!0';                           // SMTP password
		// $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 25;                                    // TCP port to connect to

		$mail->setFrom('ics@dhiraagu.com', 'ICS');
		$mail->addAddress('info@ics.com.mv', 'Yameen Mohamed'); 
		$mail->addAddress('yaamynu@gmail.com', 'Yameen Mohamed');     // Add a recipient
		// $mail->addAddress('ellen@example.com');               // Name is optional
		// $mail->addReplyTo('info@example.com', 'Information');
		// $mail->addCC('cc@example.com');
		// $mail->addBCC('bcc@example.com');

		// $mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		// $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = $_POST['inquiry_name'] . ' - Inquiry';
		$mail->Body    = 'Dear Attendant,<br>'.$_POST['inquiry_text'].'<br><br>From: <a href="mailto:' .$_POST['inquiry_email'].'">'.$_POST['inquiry_email'].'</a>';
		$mail->AltBody = $_POST['inquiry_text'].'. From: ' .$_POST['inquiry_email'];

		if($mail->send() ) //
		{
			return thanks();
		}
	}
	else
	{
		ob_start();
	  	//include the specified file
	  	include('views/form.html');
	  	//assign the file output to $content variable and clean buffer
	  	return ob_get_clean();
	}
}


function thanks() {
	ob_start();
  	//include the specified file
  	include('views/thanks.html');
  	//assign the file output to $content variable and clean buffer
  	return ob_get_clean();
}

add_shortcode('general_inquiry', 'subscribe_link_shortcode');


