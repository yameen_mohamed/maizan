<?php
/**
 * The footer for our theme.
 * This template is used to generate the footer for the theme.
 *
 * @package Purpose
 * @since Purpose 1.0
 */

?>

<div class="clear"></div>

<!-- END .container -->
</div>

<!-- BEGIN .footer -->
<div class="footer">

	<?php if ( is_active_sidebar( 'footer' ) ) { ?>

	<!-- BEGIN .row -->
	<div class="row">

		<!-- BEGIN .content -->
		<div class="content">

			<!-- BEGIN .footer-widgets -->
			<div class="footer-widgets">

				<?php if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'footer' ) ) : ?>
				<?php endif; ?>

			<!-- END .footer-widgets -->
			</div>

		<!-- END .content -->
		</div>

	<!-- END .row -->
	</div>

	<?php } ?>

	<!-- BEGIN .row -->
	<div class="row">

		<!-- BEGIN .footer-information -->
		<div class="footer-information">

			<!-- BEGIN .footer-content -->
			<div class="footer-content">

				<div class="align-left">

					<p><?php esc_html_e( 'Copyright', 'organic-purpose' ); ?> &copy; <?php echo date( esc_html__( 'Y', 'organic-purpose' ) ); ?> &middot; <?php esc_html_e( 'All Rights Reserved', 'organic-purpose' ); ?> &middot; <?php bloginfo( 'name' ); ?></p>

					<?php if ( '' != get_theme_mod( 'organic_purpose_footer_text' ) ) { ?>

						<p><?php echo get_theme_mod( 'organic_purpose_footer_text' ); ?> &middot; <a href="<?php bloginfo( 'rss2_url' ); ?>"><?php esc_html_e( 'RSS Feed', 'organic-purpose' ); ?></a> &middot; <?php wp_loginout(); ?></p>

					<?php } else { ?>

						<p><a href="http://organicthemes.com/themes/" target="_blank"><?php esc_html_e( 'Purpose Theme', 'organic-purpose' ); ?></a> <?php esc_html_e( 'by', 'organic-purpose' ); ?> <a href="http://organicthemes.com" target="_blank"><?php esc_html_e( 'Organic Themes', 'organic-purpose' ); ?></a> &middot; <a href="<?php bloginfo( 'rss2_url' ); ?>"><?php esc_html_e( 'RSS Feed', 'organic-purpose' ); ?></a> &middot; <?php wp_loginout(); ?></p>

					<?php } ?>

				</div>

				<?php if ( has_nav_menu( 'social-menu' ) ) { ?>

				<div class="align-right">

					<?php wp_nav_menu( array(
						'theme_location' => 'social-menu',
						'title_li' => '',
						'depth' => 1,
						'container_class' => 'social-menu',
						'menu_class'      => 'social-icons',
						'link_before'     => '<span>',
						'link_after'      => '</span>',
						)
					); ?>

				</div>

				<?php } ?>

			<!-- END .footer-content -->
			</div>

		<!-- END .footer-information -->
		</div>

	<!-- END .row -->
	</div>

<!-- END .footer -->
</div>

<!-- END #wrap -->
</div>

<?php wp_footer(); ?>

</body>
</html>
