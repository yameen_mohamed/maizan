<?php
/**
 * This template is used to display the image slideshow.
 *
 * @package Purpose
 * @since Purpose 1.0
 */

?>
<style>

span.slider-title {
    background: rgba(19, 19, 19, 0.6);
    padding: 40px;
    width: calc(100% - 40px);
    display: block;
    position: absolute;
    top: 37%;
    text-align: center;
    font-size: xx-large;
    color: #ccc;
}

</style>
<!-- BEGIN .slideshow gallery-slideshow -->
<div class="slideshow gallery-slideshow">

	<!-- BEGIN .flexslider -->
	<div class="flexslider loading" data-speed="<?php echo get_theme_mod( 'transition_interval', '12000' ); ?>">

		<div class="preloader"></div>

		<!-- BEGIN .slides -->
		<ul class="slides">

			<?php $data = array(
				'post_parent'		=> $post->ID,
				'post_type' 		=> 'attachment',
				'post_mime_type' 	=> 'image',
				'order'         	=> 'ASC',
				'orderby'	 		=> 'menu_order',
				'numberposts' 		=> -1,
			); ?>

			<?php
			$images = get_posts( $data ); foreach ( $images as $image ) {
				$imageurl = wp_get_attachment_url( $image->ID );
				echo '<li style="background-image: url('.$imageurl.');"><img title='.$image->post_title.' src="'.$imageurl.'" /><span class="slider-title">'.$image->post_title.'</span></li>' . "\n";
			} ?>

		<!-- END .slides -->
		</ul>

	<!-- END .flexslider -->
	</div>

<!-- END .slideshow gallery-slideshow -->
</div>
