<?php

/*
-------------------------------------------------------------------------------------------------------
	Theme License and Updater
-------------------------------------------------------------------------------------------------------
*/

// Includes the files needed for the theme updater.
if ( ! class_exists( 'EDD_Theme_Updater_Admin' ) ) {
	include( dirname( __FILE__ ) . '/theme-updater-admin.php' );
}

// Loads the updater classes.
$updater = new EDD_Theme_Updater_Admin(
	// Config settings.
	$config = array(
		'remote_api_url' => 'https://organicthemes.com', // Site where EDD is hosted.
		'item_name' => 'Purpose Theme + Support & Updates Subscription', // Name of theme.
		'theme_slug' => 'purpose', // Theme slug.
		'version' => '1.6.3', // The current version of this theme.
		'author' => 'Organic Themes', // The author of this theme.
		'download_id' => '168400', // Optional, used for generating a license renewal link.
		'renew_url' => '', // Optional, allows for a custom license renewal link.
	),
	// Strings.
	$strings = array(
		'theme-license' => __( 'Theme License', 'organic-purpose' ),
		'enter-key' => __( 'Enter your theme license key.', 'organic-purpose' ),
		'license-key' => __( 'License Key', 'organic-purpose' ),
		'license-action' => __( 'License Action', 'organic-purpose' ),
		'deactivate-license' => __( 'Deactivate License', 'organic-purpose' ),
		'activate-license' => __( 'Activate License', 'organic-purpose' ),
		'status-unknown' => __( 'License status is unknown.', 'organic-purpose' ),
		'renew' => __( 'Renew?', 'organic-purpose' ),
		'unlimited' => __( 'unlimited', 'organic-purpose' ),
		'license-key-is-active' => __( 'License key is active.', 'organic-purpose' ),
		'expires%s' => __( 'Expires %s.', 'organic-purpose' ),
		'%1$s/%2$-sites' => __( 'You have %1$s / %2$s sites activated.', 'organic-purpose' ),
		'license-key-expired-%s' => __( 'License key expired %s.', 'organic-purpose' ),
		'license-key-expired' => __( 'License key has expired.', 'organic-purpose' ),
		'license-keys-do-not-match' => __( 'License keys do not match.', 'organic-purpose' ),
		'license-is-inactive' => __( 'License is inactive.', 'organic-purpose' ),
		'license-key-is-disabled' => __( 'License key is disabled.', 'organic-purpose' ),
		'site-is-inactive' => __( 'Site is inactive.', 'organic-purpose' ),
		'license-status-unknown' => __( 'License status is unknown.', 'organic-purpose' ),
		'update-notice' => __( "Updating this theme will lose any customizations you have made. 'Cancel' to stop, 'OK' to update.", 'organic-purpose' ),
		'update-available' => __( '<strong>%1$s %2$s</strong> is available. <a href="%3$s" class="thickbox" title="%4s">Check out what\'s new</a> or <a href="%5$s"%6$s>update now</a>.', 'organic-purpose' ),
	)
);
