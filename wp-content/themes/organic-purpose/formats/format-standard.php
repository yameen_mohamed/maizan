<?php if (isset($_POST['featurevid'])){ $custom = get_post_custom($post->ID); $featurevid = $custom['featurevid'][0]; } ?>
<?php $thumb = ( has_post_thumbnail() ) ? wp_get_attachment_image_src( get_post_thumbnail_id(), 'purpose-featured-medium' ) : false; ?>

<!-- BEGIN .post class -->
<div <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<?php if ( get_post_meta($post->ID, 'featurevid', true) ) { ?>
		<div class="feature-vid"><?php echo get_post_meta($post->ID, 'featurevid', true); ?></div>
	<?php } else { ?>
		<?php if ( has_post_thumbnail()) { ?>
			<div class="feature-img page-banner radius-top" style="background-image: url(<?php echo $thumb[0]; ?>);">
				<h2 class="headline small img-headline"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php esc_attr(the_title_attribute()); ?>"><?php the_title(); ?></a></h2>
				<?php the_post_thumbnail( 'purpose-featured-medium' ); ?>
			</div>
		<?php } ?>
	<?php } ?>

	<!-- BEGIN .article -->
	<div class="article">

		<?php if ( ! has_post_thumbnail() ) { ?>
			<h2 class="headline small"><a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php esc_attr(the_title_attribute()); ?>"><?php the_title(); ?></a></h2>
		<?php } ?>
		<?php the_content(__("Read More", 'organic-purpose')); ?>

	<!-- END .article -->
	</div>

	<?php $tag_list = get_the_tag_list( __( ", ", 'organic-purpose' ) ); if ( ! empty( $tag_list ) || has_category() ) { ?>

	<!-- BEGIN .post-meta -->
	<div class="post-meta">

		<div class="align-left">
			<?php if ( comments_open() ) : ?><p><i class="fa fa-comment"></i> &nbsp;<a href="<?php the_permalink(); ?>#comments"><?php comments_number(__("Leave a Comment", 'organic-purpose'), __("1 Comment", 'organic-purpose'), '% Comments'); ?></a></p><?php endif; ?>
			<p><i class="fa fa-clock-o"></i> &nbsp;<?php _e("Posted on", 'organic-purpose'); ?> <?php the_time(__("F j, Y", 'organic-purpose')); ?> <?php _e("by", 'organic-purpose'); ?> <?php esc_url ( the_author_posts_link() ); ?></p>
		</div>

		<div class="align-right text-right">
			<p><i class="fa fa-bars"></i> &nbsp;<?php the_category(', '); ?></p>
			<p><?php $tag_list = get_the_tag_list( __( ", ", 'organic-purpose' ) ); if ( ! empty( $tag_list ) ) { ?> &nbsp; &nbsp; <i class="fa fa-tags"></i> &nbsp;<?php the_tags(''); ?><?php } ?></p>
		</div>

	<!-- END .post-meta -->
	</div>

	<?php } ?>

<!-- END .post class -->
</div>
