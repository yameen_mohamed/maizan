<?php
/**
 * This template is used to display the home page.
 *
 * @package Purpose
 * @since Purpose 1.0
 */

get_header(); ?>

<?php if ( '0' != get_theme_mod( 'category_slideshow_home' ) && '' != get_theme_mod( 'category_slideshow_home' ) ) { ?>

<!-- BEGIN .home-slider -->
<div class="home-slider">

	<!-- BEGIN .row -->
	<div class="row">

		<?php get_template_part( 'content/slider', 'featured' ); ?>

	<!-- END .row -->
	</div>

<!-- END .home-slider -->
</div>

<?php } //end if ?>

<?php if ( ( '0' != get_theme_mod( 'page_one' ) || '0' != get_theme_mod( 'page_two' ) || '0' != get_theme_mod( 'page_three' ) ) && ( '' != get_theme_mod( 'page_one' ) || '' != get_theme_mod( 'page_two' ) || '' != get_theme_mod( 'page_three' ) ) ) { ?>

<!-- BEGIN .featured-pages -->
<div class="featured-pages<?php if ( '0' == get_theme_mod( 'category_slideshow_home' ) || '' == get_theme_mod( 'category_slideshow_home' ) ) { ?> no-thumb<?php } ?>">

	<?php if ( '0' != get_theme_mod( 'page_one' ) && '' != get_theme_mod( 'page_one' ) ) { ?>

		<!-- BEGIN .row -->
		<div class="row">

		<?php $recent = new WP_Query( 'page_id='.get_theme_mod( 'page_one', '0' ) );
		while ( $recent->have_posts() ) : $recent->the_post(); ?>
				<?php global $more;
				$more = 0; ?>

			<?php get_template_part( 'content/page', 'featured' ); ?>

		<?php endwhile; ?>

		<!-- END .row -->
		</div>

	<?php } //end if ?>

	<?php if ( '0' != get_theme_mod( 'page_two' ) && '' != get_theme_mod( 'page_two' ) ) { ?>

		<!-- BEGIN .row -->
		<div class="row">

		<?php $recent = new WP_Query( 'page_id='.get_theme_mod( 'page_two', '0' ) );
		while ( $recent->have_posts() ) : $recent->the_post(); ?>
				<?php global $more;
				$more = 0; ?>

			<?php get_template_part( 'content/page', 'featured' ); ?>

		<?php endwhile; ?>

		<!-- END .row -->
		</div>

	<?php } //end if ?>

	<?php if ( '0' != get_theme_mod( 'page_three' ) && '' != get_theme_mod( 'page_three' ) ) { ?>

		<!-- BEGIN .row -->
		<div class="row">

		<?php $recent = new WP_Query( 'page_id='.get_theme_mod( 'page_three', '0' ) );
		while ( $recent->have_posts() ) : $recent->the_post(); ?>
				<?php global $more;
				$more = 0; ?>

			<?php get_template_part( 'content/page', 'featured' ); ?>

		<?php endwhile; ?>

		<!-- END .row -->
		</div>

	<?php } //end if ?>




<!-- END .featured-pages -->
</div>

<?php } //end if for featured pages section ?>

<?php if ( '0' != get_theme_mod( 'category_news' ) && '' != get_theme_mod( 'category_news' ) ) { ?>

<!-- BEGIN .featured-posts -->
<div class="featured-posts">

	<!-- BEGIN .row -->
	<div class="row">

		<?php get_template_part( 'content/post', 'featured' ); ?>

	<!-- END .row -->
	</div>

<!-- END .featured-posts -->
</div>

<?php } //end if ?>

<?php if ( ( '' == get_theme_mod( 'category_slideshow_home' ) || '0' == get_theme_mod( 'category_slideshow_home' ) ) && ( '' == get_theme_mod( 'page_one' ) || '0' == get_theme_mod( 'page_one' ) ) && ( '' == get_theme_mod( 'page_two' ) || '0' == get_theme_mod( 'page_two' ) ) && ( '' == get_theme_mod( 'page_three' ) || '0' == get_theme_mod( 'page_three' ) ) &&  ( '' == get_theme_mod( 'category_news' ) || '' == get_theme_mod( 'category_news' ) ) ) { ?>

<!-- BEGIN .set-options -->
<div class="set-options<?php if ( '0' == get_theme_mod( 'category_slideshow_home' ) || '' == get_theme_mod( 'category_slideshow_home' ) ) { ?> no-thumb<?php } ?>">

	<!-- BEGIN .row -->
	<div class="row">

		<!-- BEGIN .postarea -->
		<div class="postarea full">

			<?php get_template_part( 'content/content', 'none' ); ?>

		<!-- END .postarea -->
		</div>

	<!-- END .row -->
	</div>

<!-- END .set-options -->
</div>

<?php } ?>

<?php get_footer(); ?>
